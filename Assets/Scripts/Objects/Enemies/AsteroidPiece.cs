﻿using UnityEngine;
using System.Collections;

public class AsteroidPiece : MonoBehaviour {
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary") return;
        if (other.tag == "PlayerBullet")
        {
            Gamelogic.gamelogic.AddScore(1, transform.position);
            GameObject vfx = Instantiate(Gamelogic.gamelogic.vfx.explosion, transform.position, Quaternion.identity) as GameObject;
            Destroy(gameObject);
            Destroy(other.gameObject);
        }
    }
}
