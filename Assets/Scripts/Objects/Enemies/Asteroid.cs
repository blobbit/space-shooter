﻿using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour {

    public float speed = 1.0f;
    public int health = 110;
    public int scoreReward = 0;
    [SerializeField]
    private GameObject fragmentPrefab;

    private Material mat;

    void Start()
    {
        //inside unit sphere = random vector3
        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * speed;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary") return;
        if (other.tag == "PlayerBullet" || other.tag == "EnemyBullet")
        {

            health -= other.GetComponent<Bullet>().damage;

            GameObject vfx = Instantiate(Gamelogic.gamelogic.vfx.hit, other.transform.position, Quaternion.identity) as GameObject;
            vfx.transform.SetParent(transform);
            Instantiate(Gamelogic.gamelogic.vfx.hit_asteroid, vfx.transform.position, Quaternion.identity);

            if (health <= 0)
            {
                Gamelogic.gamelogic.AddScore(scoreReward, transform.position);
                SpawnFragments();
                Destroy(gameObject);
            }

            Destroy(other.gameObject);
        }
    }

    void SpawnFragments()
    {
        GameObject vfx = Instantiate(Gamelogic.gamelogic.vfx.explosion, transform.position, Quaternion.identity) as GameObject;
        Vector3 dir = Vector3.up;

        for (int i = 0; i < 5; i++)
        {
            GameObject fragment = Instantiate(fragmentPrefab, transform.position, transform.rotation) as GameObject;
            Gamelogic.gamelogic.GetComponent<Spawner>().AddEnemy(ref fragment);
            Rigidbody rb = fragment.GetComponent<Rigidbody>();
            rb.rotation = Random.rotation;
            rb.velocity = dir * 20;

            dir = Quaternion.Euler(0, 0, -72) * dir;
        }
    }
}
