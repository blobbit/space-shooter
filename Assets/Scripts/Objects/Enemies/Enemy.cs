﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
    [Tooltip("0 = standard behaviour")]
    public int behaviour = 0;
    public float speed = 1.0f;
    public int health = 110;
    public int scoreReward = 0;

    private Vector3 velocity = Vector3.zero;
    private Vector3 targetPosition; //given by the Spawner to move enemy to the target position
    private bool moving = false; //is movin to the target position

    [SerializeField]
    private GameObject bullet;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary") return;
        if (other.tag == "PlayerBullet")
        {

            health -= other.GetComponent<Bullet>().damage;

            GameObject vfx = Instantiate(Gamelogic.gamelogic.vfx.hit, other.transform.position, Quaternion.identity) as GameObject;
            vfx.transform.SetParent(transform);
            Instantiate(Gamelogic.gamelogic.vfx.hit_asteroid, vfx.transform.position, Quaternion.identity);

            if (health <= 0)
            {
                Gamelogic.gamelogic.AddScore(scoreReward, transform.position);
                Destroy(gameObject);
            }

            Destroy(other.gameObject);
        }
    }

    private float nextShoot = 0f;

    void Update()
    {
        if (moving)
        {
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, 0.3f);
            if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
                moving = false;
        } else if (nextShoot <= 0f)
        {
            GameObject newBullet = Instantiate(bullet, transform.position, Quaternion.identity) as GameObject;
            Gamelogic.gamelogic.GetComponent<Spawner>().AddEnemy(ref newBullet);

            switch (behaviour)
            {
                case 0:
                    GetComponent<Rigidbody>().velocity = Vector3.right * speed;
                    break;
                case 1:
                    GetComponent<Rigidbody>().velocity = Vector3.left * speed;
                    break;
            }

            nextShoot = Random.Range(3f,4f);
        } else
        {
            nextShoot -= Time.deltaTime;
        }
    }

    public void MoveTo(Vector3 targetPos)
    {
        moving = true;
        targetPosition = targetPos;
    }
}
