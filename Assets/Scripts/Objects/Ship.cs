﻿using UnityEngine;

public class Ship : MonoBehaviour {
    [SerializeField]
    private string[] perkModules = new string[5];

    public string GetModuleColorID(int id)
    {
        return perkModules[id];
    }
}