﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
    public float speed, tilt;
    [HideInInspector]
    public bool createShipSelector = false, canMove = false;

    private Boundary boundary;
    private Rigidbody rBody;
    private float goal = 0f;

    [SerializeField] private GameObject shieldPrefab;
    private GameObject shield, shipSelector;

    void Start()
    {
        boundary = Gamelogic.gamelogic.boundary;
        if (rBody == null) rBody = GetComponent<Rigidbody>();
        MoveToScreen();
    }

    void FixedUpdate ()
    {
        if (rBody == null) return;
        if (canMove) Move();
        if (goal != 0f) MoveTowardsGoal();
    }

    void MoveTowardsGoal()
    {
        rBody.velocity = Vector3.up * speed * 1.5f;
        if (rBody.position.y >= goal)
        {
            if (createShipSelector)
            {
                shipSelector = Instantiate(Gamelogic.gamelogic.menus.shipSelectorCanvas) as GameObject;
                shipSelector.transform.SetParent(gameObject.transform);
                shipSelector.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, -3.7f);

                if (!Tutorials.Progress.ShipChangeCompleted)
                    Tutorials.CreateTutorial(Tutorials.Prefabs.ShipChangeTutorialPrefab, shipSelector.transform);
                else if (!Tutorials.Progress.ShipModifyCompleted)
                    Tutorials.CreateTutorial(Tutorials.Prefabs.ShipModifyTutorialPrefab, shipSelector.transform);

                createShipSelector = false;
            }
            rBody.velocity = Vector3.zero;
            goal = 0f;
        }
    }

    public void RemoveShipSelector()
    {
        GameObject.Destroy(shipSelector);
    }

    void Move()
    {
        float moveH = Input.GetAxis("Horizontal"),
        moveV = Input.GetAxis("Vertical");

        Vector3 move = new Vector3(moveH, moveV, 0f);


        rBody.velocity = move * speed;
        rBody.position = new Vector3(Mathf.Clamp(rBody.position.x, boundary.xMin, boundary.xMax),
                                        Mathf.Clamp(rBody.position.y, boundary.yMin, boundary.yMax), 0f);
        rBody.rotation = Quaternion.Euler(0f, rBody.velocity.x * -tilt, 0f);
    }

    void MoveToScreen()
    {
        rBody.position = new Vector3(rBody.position.x, boundary.xMin - 20f, 0f);
        goal = boundary.xMin + 5f;
    }

    public void MoveOffScreen()
    {
        rBody.velocity = Vector3.up * speed * 2f;
        Destroy(GetComponent<PlayerShooting>());
        Destroy(gameObject, 5f);
        Destroy(this);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary") return;
        if (other.tag == "EnemyBullet")
        {
            GameObject vfx = Instantiate(Gamelogic.gamelogic.vfx.explosion, transform.position, Quaternion.identity) as GameObject;
            Gamelogic.gamelogic.GameOver();
            Destroy(gameObject);
        }
    }

    public void RemoveShield()
    {
        if (shield != null)
            GameObject.Destroy(shield);
    }

    public void CreateShield()
    {
        GameObject shield = Instantiate(shieldPrefab) as GameObject;
        shield.transform.SetParent(gameObject.transform);
    }
}
