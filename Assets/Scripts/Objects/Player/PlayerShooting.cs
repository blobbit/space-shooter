﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour {

    public GameObject bullet;
    [SerializeField]
    private Transform[] simultaneousBarrels;
    [SerializeField]
    private Transform[] alternatingBarrels;
    [SerializeField]
    private float simultaneousFireRate, alternatingFirerate;
    private float nextSimultaneous, nextAlternate;
    private int nextBarrel = 0;

	void Update () {
        if (!GetComponent<PlayerMovement>().canMove) return;

        ShootAlternatingBarrels();
        ShootSimultaneousBarrels();
	}

    void ShootSimultaneousBarrels()
    {
        if (Input.GetButton("Fire1") && Time.time > nextSimultaneous && simultaneousBarrels.Length > 0)
        {
            nextSimultaneous = Time.time + simultaneousFireRate;

            foreach (Transform t in simultaneousBarrels)
            {
                CreateBullet(t);
            }
        }
    }

    void ShootAlternatingBarrels()
    {
        if (Input.GetButton("Fire1") && Time.time > nextAlternate && alternatingBarrels.Length > 0)
        {
            nextAlternate = Time.time + alternatingFirerate;

            if (nextBarrel >= alternatingBarrels.Length) nextBarrel = 0;

            CreateBullet(alternatingBarrels[nextBarrel]);

            nextBarrel++;
        }
    }


    void CreateBullet(Transform t)
    {
        GameObject newBullet = Instantiate(bullet, t.position, Quaternion.identity) as GameObject;
        newBullet.GetComponent<Bullet>().enemy = false;
    }
}
