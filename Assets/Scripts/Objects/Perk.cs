﻿using UnityEngine;
using System.Collections;

public class Perk : MonoBehaviour {

    public Sprite icon;
    public string colorID = "yellow"; //yellow for generic, red for weapons etc.
    public string tooltipHeader;
    [TextArea(3, 10)]
    public string tooltipContent;

    public void Clone(Perk perk)
    {
        this.icon = perk.icon;
        this.colorID = perk.colorID;
        this.tooltipHeader = perk.tooltipHeader;
        this.tooltipContent = perk.tooltipContent;
    }

    public static Color GetColorByID(string id) {
        switch(id)
        {
            case "grey":
                return Color.grey;
            case "yellow":
                return Color.yellow;
            case "red":
                return Color.red;
            case "green":
                return Color.green;
            case "blue":
                return Color.blue;
            default:
                return Color.grey;
        }
    }
}
