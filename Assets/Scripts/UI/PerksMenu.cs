﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PerksMenu : MonoBehaviour {

    [SerializeField]
    private GameObject perkPrefab;
    [SerializeField] //locations for perkModuleSlots
    private RectTransform[] perkModules = new RectTransform[5];

    private Perk perkSelected;

	void Start ()
    {
        DrawPerks();
        DrawPerkModules();
    }

    void DrawPerkModules()
    {
        for (int i = 0; i < perkModules.Length; i++)
        {
            string colorID = Gamelogic.gamelogic.ship.GetModuleColorID(i);

            if (colorID == "" || colorID == "grey")
            {
                perkModules[i].GetComponent<Toggle>().interactable = false;
            } else
            {
                perkModules[i].GetComponent<Image>().color = Perk.GetColorByID(colorID);
            }
        }
    }

    public void ModuleClicked(RectTransform rt)
    {

    }

    public void PerkClicked(Perk perk)
    {
        perkSelected = perk;
    }


    //TODO: alalaidassa näkyy shipin perk slotit, slotit ovat toggle valinnassa ja tietynväriseen slottiin voi ottaa vain tietyn värisiä perkkejä. Yellow / harmaa slotteihin voi ottaa mitä tahansa väriä ja Yellow / harmaa perkit menee kaikkiin. Jos perkkiä ei voi valita se näkyy harmaana. Mustana voisi näkyä lockatut perkit. (Mustana ja lukko ikonina, tool Tippiin requirements tai seeeecret)
    void DrawPerks()
    {
        Perk[] unlockedperkslist = Gamelogic.gamelogic.perksProgress.unlockedPerks;

        float x = -8f;
        float y = GetComponent<RectTransform>().sizeDelta.y / 2.7f;

        for (int i = 0; i < 36; i++)
        {
            GameObject p = Instantiate(perkPrefab, Vector2.zero, Quaternion.identity) as GameObject;
            p.transform.SetParent(transform);

            Perk perk;
            if (i < unlockedperkslist.Length)
                 perk = unlockedperkslist[i];
                 else
            {
                perk = Gamelogic.gamelogic.objectPrefabs.lockedPerkPrefab;
                p.GetComponent<Button>().interactable = false;
            }
                    

            p.AddComponent<Perk>().Clone(perk);
            p.GetComponent<Button>().onClick.AddListener(() => { PerkClicked(perk); });

            x += p.GetComponent<RectTransform>().sizeDelta.x + 5f;

            if (x > GetComponent<RectTransform>().sizeDelta.x)
            {
                x = p.GetComponent<RectTransform>().sizeDelta.x -3f; // reset x if goes over the border, and adds another row +5f - 8f = -3f
                y -= p.GetComponent<RectTransform>().sizeDelta.y + 5f;
            }

            p.GetComponent<RectTransform>().anchoredPosition = new Vector2(x, y);
        }
    }

    public void ReturnToMainMenu()
    {
        Gamelogic.gamelogic.ChangeMenu(Gamelogic.gamelogic.menus.mainMenuPrefab);
    }
}
