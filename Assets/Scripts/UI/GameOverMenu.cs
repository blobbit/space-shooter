﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverMenu : MonoBehaviour {
    public Text score;
    [SerializeField]
    private GameObject unlocksPanel, contentPanel, perkUnlockedPrefab;
    private int unlocks = 1; //test for checking if anything unlocked

    void Start()
    {
        if (unlocks == 0)
            GameObject.Destroy(unlocksPanel);
        else ShowUnlockedPerks();

        Tutorials.CreateTutorial(Tutorials.Prefabs.SavingMsg, GetComponentInParent<Canvas>().transform);
        if (GamePersistence.SaveGame()) Debug.Log("Game Saved!");
        else Debug.LogError("Error saving the game!");
    }

    void Update()
    {
        if (Input.anyKeyDown)
        {
            Gamelogic.gamelogic.ChangeMenu(Gamelogic.gamelogic.menus.mainMenuPrefab);
            this.enabled = false;
        }
    }

    void ShowUnlockedPerks()
    {
        RectTransform contentRect = contentPanel.GetComponent<RectTransform>();
        float perksize = perkUnlockedPrefab.GetComponent<RectTransform>().sizeDelta.x;
        float margin = 2f;
        contentRect.sizeDelta = new Vector2(unlocks * (perksize + margin), contentRect.sizeDelta.y);
        if (unlocks < 4)
        {
            unlocksPanel.GetComponentInChildren<ScrollRect>().content = new GameObject().AddComponent<RectTransform>();
            contentRect.anchoredPosition = new Vector2(-((unlocks * (perksize + margin)) / 2), contentRect.anchoredPosition.y);
            Mask m = unlocksPanel.GetComponentInChildren<Mask>();
            m.enabled = false;
            m.GetComponent<Image>().enabled = false;
        }
        for (int i = 0; i < unlocks; i++)
        {
            GameObject perk = Instantiate(perkUnlockedPrefab) as GameObject;
            perk.transform.SetParent(contentPanel.transform);
            RectTransform rt = perk.GetComponent<RectTransform>();
            rt.localScale = Vector3.one;
            rt.anchoredPosition = new Vector3((perksize / 2) + margin + i * (rt.sizeDelta.x + margin), 0f, 0f);
            perk.AddComponent<Perk>().Clone(Gamelogic.gamelogic.perksProgress.unlockedPerks[0]);
            perk.GetComponent<Button>().interactable = false;
        }
    }
}
