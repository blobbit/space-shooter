﻿using UnityEngine;
using UnityEngine.UI;

public class PerkCosmetics : MonoBehaviour
{

    public Image Icon, Frame;
    private Perk perk;

    private GameObject tooltip;

    private void CreateToolTip()
    {
        tooltip = Instantiate(Gamelogic.gamelogic.perksProgress.toolTipPrefab, Vector2.zero, Quaternion.identity) as GameObject;
        ToolTip tp = tooltip.GetComponent<ToolTip>();
        tp.Initialize(perk.tooltipHeader, perk.tooltipContent);
        tp.headerText.color = Perk.GetColorByID(perk.colorID);

        tooltip.transform.SetParent(transform.parent.parent.transform);

        RectTransform tooltipRect = tooltip.GetComponent<RectTransform>();
        tooltipRect.localScale = Vector2.one;

        Vector2 pos = GetComponent<RectTransform>().position;
        Vector2 size = GetComponent<RectTransform>().sizeDelta;
        pos.y -= tooltipRect.sizeDelta.y + size.y*2;
        pos.x += tooltipRect.sizeDelta.x + size.x*2;

        //TODO: check if tooltip goes over the border / over middle line, change its position to other side

        tooltipRect.position = pos;
    }

    public void PointerEnter()
    {
        if (tooltip == null) CreateToolTip();
    }

    public void PointerExit()
    {
        if (tooltip != null)
        {
            tooltip.GetComponent<Image>().CrossFadeAlpha(0f, .3f, false);

            Text[] txts = tooltip.GetComponentsInChildren<Text>();

            foreach (Text t in txts)
            {
                t.CrossFadeAlpha(0f, .3f, false);
            }

            Destroy(tooltip, 0.3f);
            tooltip = null;
        }
    }

    void Start()
    {
        GetComponent<RectTransform>().localScale = Vector2.one;

        perk = GetComponent<Perk>();

        Frame.color = Perk.GetColorByID(perk.colorID);
        Icon.sprite = perk.icon;
    }
}
