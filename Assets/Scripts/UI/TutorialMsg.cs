﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialMsg : MonoBehaviour {

    [SerializeField] private Image arrow;
    [SerializeField] private Text message;

    static float minalpha = .3f, maxalpha = .7f;

    void Start()
    {
        arrow.CrossFadeAlpha(0,0,true);
        message.CrossFadeAlpha(0,0,true);
        StartCoroutine(WaitAndFadeIn());
    }

    IEnumerator WaitAndFadeIn()
    {
        yield return new WaitForSeconds(1);
        arrow.CrossFadeAlpha(maxalpha, 1, true);
        message.CrossFadeAlpha(maxalpha, 1, true);
        StartCoroutine(WaitAndFadeOut());
    }
    
    IEnumerator WaitAndFadeOut()
    {
        yield return new WaitForSeconds(1);
        arrow.CrossFadeAlpha(minalpha, 1, true);
        message.CrossFadeAlpha(minalpha, 1, true);
        StartCoroutine(WaitAndFadeIn());
    }
}
