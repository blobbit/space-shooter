﻿using UnityEngine;
using System.Collections;

public class ShipSelector : MonoBehaviour {

    public void NextShip()
    {
        Tutorials.Progress.ShipChangeCompleted = true;
        Gamelogic.gamelogic.ChangeShip(1);
    }

    public void PreviousShip()
    {
        Tutorials.Progress.ShipChangeCompleted = true;
        Gamelogic.gamelogic.ChangeShip(-1);
    }

    public void ModifyShip()
    {
        Gamelogic.gamelogic.ChangeMenu(Gamelogic.gamelogic.menus.perksMenuPrefab);
        Gamelogic.gamelogic.RemoveShip();
        Tutorials.Progress.ShipModifyCompleted = true;
    }
}
