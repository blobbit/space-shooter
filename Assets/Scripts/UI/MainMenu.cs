﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    void Start()
    {
        Gamelogic.gamelogic.CreateShipSelection();
    }

    public void StartGameButton()
    {
        Gamelogic.gamelogic.StartNewGame();
    }

    public void OptionsButton()
    {

    }

    public void QuitGameButton()
    {
        //TODO: ios version doesn't close
        Application.Quit();
    }
}
