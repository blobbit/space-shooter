﻿using UnityEngine;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour {

    public Text headerText, contentText;

    public void Initialize(string header, string content)
    {
        headerText.text = header;
        contentText.text = content;
    }

    void Start()
    {
        GetComponent<Image>().CrossFadeAlpha(0, 0, false);
        GetComponent<Image>().CrossFadeAlpha(1f, .4f, false);

        Text[] txts = GetComponentsInChildren<Text>();

        foreach (Text t in txts)
        {
            t.CrossFadeAlpha(0, 0, false);
            t.CrossFadeAlpha(1f, .4f, false);
        } 
    }
}
