﻿using UnityEngine;
using System.Collections;

public class BackGround : MonoBehaviour {

    public float speed;
    public bool hasCopy = false;

	void Start () {
	
	}
	
	void Update () {
        transform.position = new Vector3(transform.position.x, transform.position.y - speed, transform.position.z);

        if (transform.position.y <= -150f && !hasCopy)
        {
            hasCopy = true;
            GameObject copy = GameObject.Instantiate(gameObject, new Vector3(transform.position.x, 450f, transform.position.z), transform.rotation) as GameObject;
            BackGround bg = copy.GetComponent<BackGround>();
            bg.hasCopy = false;
            bg.speed = speed;
        }

        if (transform.position.y <= -450f)
            Destroy(gameObject);
    }
}
