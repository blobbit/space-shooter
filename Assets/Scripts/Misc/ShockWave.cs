﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShockWave : MonoBehaviour {

    private SpriteRenderer sr;
    private float lifeTime = .4f;
    [SerializeField] private float power = 1f;

	void Start () {
        sr = GetComponent<SpriteRenderer>();
        transform.localScale = Vector3.zero;
    }

	void Update () {
        sr.color = new Color(255,255,255,lifeTime*2);
        transform.localScale += new Vector3(power, power, power);

        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0) Destroy(gameObject);
    }
}
