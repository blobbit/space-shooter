﻿using UnityEngine;

//Tool class for making timed destroying easier in inspector.
//In code you can use Destroy(object, delay);
public class SelfDestruct : MonoBehaviour {

    [SerializeField]
    private float lifeTime = 2f;

	void Update () {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0) Destroy(gameObject);
    }
}