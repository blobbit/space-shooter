﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    public float speed;
    public int damage;
    public bool enemy = true;

	void Start () {
        if (enemy) gameObject.tag = "EnemyBullet"; else gameObject.tag = "PlayerBullet";
        if (speed != 0) GetComponent<Rigidbody>().velocity = transform.up * speed;
	}
}
