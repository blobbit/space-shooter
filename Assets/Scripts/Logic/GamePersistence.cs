﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

//class maintaining game persistence, saving loading etc. TODO: reset progress
public static class GamePersistence {

    static string path = Application.persistentDataPath + "/SpaceRewindSave.dat";

    public static bool SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(path, FileMode.OpenOrCreate);

        SaveData save = new SaveData();
        save.tutorials = Tutorials.Progress;
        //save.ships = Gamelogic.gamelogic.shipsProgress;

        bf.Serialize(file, save);
        file.Close();
        return true;
    }

    public static void Load()
    {
        if (File.Exists(path))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(path, FileMode.Open);
            SaveData save = (SaveData)bf.Deserialize(file);
            file.Close();
            Tutorials.Progress = save.tutorials;
            //Gamelogic.gamelogic.shipsProgress = save.ships;
        }
    }
}

[System.Serializable]
class SaveData {
    public TutorialProgress tutorials { get; set; }
    public ShipsProgress ships { get; set; }
}