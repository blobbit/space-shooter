﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {

    [SerializeField]
    private GameObject asteroidPrefab, alien2Prefab;
    [SerializeField] private float spawnrate;
    private float next; //time when next enemy spawns
    private GameObject spawner; //empty GameObject to position new enemies

    public bool spawning = false;
    private List<GameObject> enemies; //entities such as enemies / enemybullets currently alive.

    private Vector3 movePos = Vector3.zero; //where next enemy will move after spawn, used to create formations etc.
    private int nextbehaviour = 0; //what behaviour will be assigned to the next enemy

    void Start()
    {
        enemies = new List<GameObject>();
        spawner = new GameObject();
        spawner.name = "Spawner";

        movePos = new Vector2(Gamelogic.gamelogic.boundary.xMin + 5f, Gamelogic.gamelogic.boundary.yMax);
    }

    void Update()
    {
        SpawnWave();
    }


    void SpawnWave()
    {
        if (!spawning) return;
        if (Time.time > next)
        {
            next = Time.time + spawnrate;

            float x = Random.Range(Gamelogic.gamelogic.boundary.xMin, Gamelogic.gamelogic.boundary.xMax);
            float y = Gamelogic.gamelogic.boundary.yMax + 20;

            spawner.transform.position = new Vector3(x, y, 0f);

            enemies.Add(CreateAlien());
        }
    }

    private GameObject CreateAlien()
    {
        GameObject g = Instantiate(alien2Prefab, new Vector3(spawner.transform.position.x, spawner.transform.position.y, -20f), alien2Prefab.transform.rotation) as GameObject;
        Enemy e = g.GetComponent<Enemy>();
        e.MoveTo(movePos);
        e.scoreReward = e.health / 10;
        e.behaviour = nextbehaviour;

        movePos.x += 15f;

        //todo: smarter formation system
        if (movePos.x > Gamelogic.gamelogic.boundary.xMax - 5f)
        {
            if (nextbehaviour == 1) nextbehaviour = 0;
            else nextbehaviour = 1;

            movePos.x = Gamelogic.gamelogic.boundary.xMin + 5f;

            movePos.y -= 15f;
            if (movePos.y < 0f) //if y goes under middle reset
                movePos.y = Gamelogic.gamelogic.boundary.yMax - 5f;
        }

        return g;
    }

    public void AddEnemy(ref GameObject g)
    {
        enemies.Add(g);
    }

    public void ClearScreen()
    {
        foreach (GameObject g in enemies)
        {
            GameObject.Destroy(g);
        }
    }
}
