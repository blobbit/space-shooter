﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TutorialProgress
{
    public bool ShipChangeCompleted = false,
                ShipModifyCompleted = false;
}

public static class Tutorials {
    public static TutorialProgress Progress = new TutorialProgress();
    public static TutorialPrefabs Prefabs = new TutorialPrefabs();

    public static void CreateTutorial(GameObject tutorialPrefab, Transform parent)
    {
        GameObject t = GameObject.Instantiate(tutorialPrefab) as GameObject;
        t.transform.SetParent(parent);
        t.transform.localPosition = tutorialPrefab.transform.position;
        t.transform.localScale = tutorialPrefab.transform.localScale;
    }
}
