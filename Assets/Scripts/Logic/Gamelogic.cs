﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

#region Serializable Classes
[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;
}

[System.Serializable]
public class VFX
{
    public GameObject explosion, hit, score, hit_asteroid;
}

[System.Serializable]
public class PerksProgress
{
    public Perk[] unlockedPerks;
    public GameObject toolTipPrefab;
}

[System.Serializable]
public class ObjectPrefabs
{
    public GameObject[] shipPrefabs;
    public Perk[] perkPrefabs;
    public Perk lockedPerkPrefab;
}

[System.Serializable]
public class ShipsProgress
{
    public Ship[] unlockedShips;
}

[System.Serializable]
public class Menus
{
    public GameObject UIPanel, mainMenuPrefab, gameOverMenuPrefab, perksMenuPrefab, shipSelectorCanvas;
}

[System.Serializable]
public class TutorialPrefabs
{
    public GameObject ShipChangeTutorialPrefab, ShipModifyTutorialPrefab;
    public GameObject SavingMsg;
}
#endregion

public class Gamelogic : MonoBehaviour {
    #region VARIABLES
    //prefabs stored here
    public Boundary boundary;
    public VFX vfx;
    public PerksProgress perksProgress;
    public ShipsProgress shipsProgress;
    public ObjectPrefabs objectPrefabs;
    public Menus menus;
    public TutorialPrefabs tutorialsPrefabs;
    public Ship ship { get; set; }

    //'overlord' classes:
    public static Gamelogic gamelogic;

    private Spawner spawner;
    
    //class private variables
    private int score = 0;
    [SerializeField]
    private Animator animator;
    [SerializeField] private GameObject bg1, bg2;

    //player stuff / other private variables
    private GameObject player;
    private PlayerMovement playerMovement;
    private float bg1_speed = .03f, bg2_speed = .06f;
    private int index = 0;

    private GameObject currentMenu;
    #endregion
    void Awake()
    {
        if (gamelogic == null) gamelogic = this;
        GamePersistence.Load();
        Tutorials.Prefabs = tutorialsPrefabs;
    }

    void Start()
    {
        spawner = GetComponent<Spawner>();
        ChangeMenu(menus.mainMenuPrefab);
    }

    void Update()
    {
        UIButtons();
    }

    public void CreateShipSelection()
    {
        player = Instantiate(objectPrefabs.shipPrefabs[index], new Vector3(0f,boundary.xMin - 20f), transform.rotation) as GameObject;
        (playerMovement = player.GetComponent<PlayerMovement>()).createShipSelector = true;
        ship = player.GetComponent<Ship>();
    }

    public void ChangeShip(int dir)
    {
        RemoveShip();

        index += dir;
        if (index > objectPrefabs.shipPrefabs.Length - 1) index = 0;
        if (index < 0) index = objectPrefabs.shipPrefabs.Length - 1;

        CreateShipSelection();
    }

    public void RemoveShip()
    {
        playerMovement.MoveOffScreen();
        playerMovement.RemoveShipSelector();
    }

    public void GameOver()
    {
        GameObject gameOverMenu = CreateMenu(menus.gameOverMenuPrefab);
        gameOverMenu.GetComponent<GameOverMenu>().score.text = "score: " + score;
        spawner.spawning = false;
        spawner.ClearScreen();
        playerMovement.canMove = false;
        Cursor.visible = true;
    }

    void UIButtons()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit(); //TODO: open up pause menu (almost like mainmenu but has resume, mainmenu and options buttons)
    }

    public void StartNewGame()
    {
        HideMenu();
        spawner.spawning = true;

        playerMovement.canMove = true;
        playerMovement.RemoveShipSelector();
        Cursor.visible = false;
    }

    public void AddScore(int amount, Vector3 pos)
    {
        score += amount;

        pos.z = 0f;

        GameObject g = Instantiate(Gamelogic.gamelogic.vfx.score, pos, Quaternion.identity) as GameObject;
        Text t = g.GetComponentInChildren<Text>();
        t.text = "+" + amount;
        if (amount < 5) t.fontSize = 150;
        else if (amount >= 500) t.fontSize = 400;
        else if (amount >= 200) t.fontSize = 350;
        else if (amount >= 100) t.fontSize = 300;
        else if (amount >= 20) t.fontSize = 250;
        else if (amount >= 5) t.fontSize = 200;

        t.CrossFadeAlpha(0f,5f,false);
        GameObject.Destroy(g, 5f);
    }

    private void HideMenu()
    {
        currentMenu.AddComponent<SelfDestruct>();
        animator.SetBool("UI", false);
    }

    private GameObject CreateMenu(GameObject menu)
    {
        if (currentMenu != null) GameObject.Destroy(currentMenu); //to make sure
        currentMenu = Instantiate(menu);
        currentMenu.transform.SetParent(menus.UIPanel.transform);
        currentMenu.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        currentMenu.GetComponent<RectTransform>().localScale = Vector3.one;
        animator.SetBool("UI", true);
        return currentMenu;
    }

    public void ChangeMenu(GameObject menu)
    {
        StartCoroutine(ChangeMenuCoroutine(menu));
    }

    private IEnumerator ChangeMenuCoroutine(GameObject menu)
    {
        if (currentMenu == null)
        {
            CreateMenu(menu);
        }
        else
        {
            animator.SetBool("UI", false);
            yield return new WaitForSeconds(1);
            GameObject.Destroy(currentMenu);
            CreateMenu(menu);
        }
    }
}
